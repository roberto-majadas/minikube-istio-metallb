#!/bin/bash

#
# Start minikube
#
################################

minikube start --memory=8192 --cpus=4 --kubernetes-version=v1.14.2


#
# Download istio
#
################################

ISTIO_VERSION=1.1.7
ISTIO_URL="https://github.com/istio/istio/releases/download/${ISTIO_VERSION}/istio-${ISTIO_VERSION}-linux.tar.gz"

echo "Downloading istio-$ISTIO_VERSION"
curl -# -L "$ISTIO_URL" | tar xz
rm -fr istio/
mv istio-$ISTIO_VERSION istio


#
# Install helm tiller
#
################################

kubectl apply -f k8s/rbac/tiller.yml
helm init --service-account tiller --wait


#
# Install istio with helm
#
################################

kubectl create namespace istio-system
kubectl apply -f k8s/secrets/kiali.yml

for i in istio/install/kubernetes/helm/istio-init/files/crd*yaml; do kubectl apply -f $i; done

helm install istio/install/kubernetes/helm/istio --name istio --namespace istio-system --values k8s/values/istio.yml
kubectl label namespace default istio-injection=enabled

kubectl apply -f k8s/gateways/kiali.yml


#
# Install metallb
#
################################

helm install --namespace metallb-system --name metallb -f k8s/values/metallb.yml stable/metallb

#
# Create monitoring namespace
#
################################
kubectl create namespace monitoring
kubectl label namespace monitoring istio-injection=enabled

#
# Install prometheus
#
################################

helm install --name prom-mon --namespace monitoring --values k8s/values/prometheus.yml stable/prometheus

#
# Install grafana
#
################################

kubectl apply -f k8s/gateways/grafana.yml
kubectl apply -f k8s/secrets/grafana.yml
kubectl apply -f k8s/configmaps/grafana-prometheus-datasource.yml
for i in k8s/dashboards/k8s-*.yml; do kubectl apply -f $i; done
for i in k8s/dashboards/istio-*.yml; do kubectl apply -f $i; done
helm install --name grafana-mon --namespace monitoring -f k8s/values/grafana.yml stable/grafana

#
# Install kubernetes dashboard
#
################################
#
# kubectl create clusterrolebinding kubernetes-dashboard --clusterrole=cluster-admin --serviceaccount=kube-system:kubernetes-dashboard
# helm install --wait --name=kubernetes-dashboard --set enableInsecureLogin=true stable/kubernetes-dashboard --namespace=kube-system
#
# echo "---------------------------"
# DASHBOARD_TOKEN=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep kubernetes-dashboard-token | head -1 | awk '{print $1}')  | grep token: | awk '{print $2}')
# echo "DASHBOARD TOKEN: $DASHBOARD_TOKEN"
# echo "---------------------------"
